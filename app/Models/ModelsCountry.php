<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelsCountry extends Model
{
    protected $table = 'models_countries';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The date attributes from the model.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];
}
