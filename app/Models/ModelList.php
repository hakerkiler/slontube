<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelList extends Model
{
    protected $table = 'models';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'link',
        'name',
        'age',
        'aliases',
        'from_where',
        'growth',
        'weight',
        'figure',
        'hair',
        'eye_color',
        'description',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The date attributes from the model.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'object_id', 'users');
    }
}
