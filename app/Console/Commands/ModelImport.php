<?php

namespace App\Console\Commands;

use App\Models\ModelList;
use App\Models\ModelsCountry;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Yangqi\Htmldom\Htmldom;

class ModelImport extends Command
{

    public $adress_parse = 'http://www.slontube.net/pornomodels/';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'model:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import model information from http://www.slontube.net/pornomodels/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $html = new Htmldom($this->adress_parse);

        $all_models = [];
        // Find all models
        foreach ($html->find('div.th-holder2 a') as $element){
            echo $element->href . '<br>';
            $all_models[] = $this->getInformationModels($element->href);
        }

        foreach($all_models as $model){
            $find_model = ModelList::where('link', $model['link'])->first();
            if(!is_object($find_model)){
                $mod = new ModelList();
                $mod->img = $model['img'];
                $mod->link = $model['link'];
                $mod->dob = $model['data']['rodilas'];
                $mod->name = $model['name'];
                $mod->age = isset($model['data']['vozrast']) ? $model['data']['vozrast'] : '';
                $mod->aliases = isset($model['data']['psevdonimy']) ? $model['data']['psevdonimy'] : '';

                if(isset($model['data']['otkuda'])){
                    $countries = explode('/', $model['data']['otkuda']);

                    foreach($countries as $country){
                        $country_find = ModelsCountry::where('name', trim($country))->first();
                        if(!is_object($country_find)){
                            $new_country = new ModelsCountry();
                            $new_country->name = trim($country);
                            $new_country->save();
                        }
                    }
                }

                $mod->from_where = isset($model['data']['otkuda']) ? $model['data']['otkuda'] : '';
                $mod->growth = str_replace("/([^0-9\/+]+)/", "", isset($model['data']['rost']) ? $model['data']['rost'] : '' );
                $mod->weight = str_replace("/([^0-9\/+]+)/", "", isset($model['data']['ves']) ? $model['data']['ves'] : '' );
                $mod->figure = isset($model['data']['figura']) ? $model['data']['figura'] : '';
                $mod->hair = isset($model['data']['volosy']) ? $model['data']['volosy'] : '';
                $mod->eye_color = isset($model['data']['tsvet-glaz']) ? $model['data']['tsvet-glaz'] : '';
                $mod->description = isset($model['description']) ? $model['description'] : '';

                $mod->save();
            }
        }

    }

    public function getInformationModels($link)
    {

        $html = new Htmldom($link);

        $arr_return = [];
        // Find all images
        foreach ($html->find('div.img-left img') as $element){
            copy($element->src , public_path().'/upload/'.pathinfo($element->src, PATHINFO_BASENAME));
            $arr_return['img'] = pathinfo($element->src, PATHINFO_BASENAME);
        }


//      find information about model and parse
        $info_data = [];
        foreach($html->find('big') as $el){
            $list_info = explode('<br>',trim($el->innertext));
            foreach($list_info as $data){
                $clear_html = explode(':', strip_tags($data));
                if(count($clear_html) == 1){
                    $arr_return['name'] = strip_tags($data);
                } else {
                    $info_data[Str::slug($clear_html[0])] = $clear_html[1];
                }
            }
        }

        //find description
        foreach($html->find('div.widecol small') as $el){
            $arr_return['description'] = $el->innertext;
        }

        $link = str_replace($this->adress_parse,'', $link);
        $link = str_replace('/','',$link);

        $arr_return['data'] = $info_data;
        $arr_return['link'] = $link;

        return $arr_return;
    }
}
