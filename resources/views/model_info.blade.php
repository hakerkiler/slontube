<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title text-center">{{ $model->name }}</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-6">
            <img src="/upload/{{ $model->img }}" class="img-responsive center-block">
        </div>
        <div class="col-sm-6">
            {!!  $model->aliases ? '<b>Псевдонимы<b> : '. $model->aliases . '<br>' : ''  !!}
            {!!  $model->dob ? '<b>Родилась<b> : '. $model->dob . '<br>' : ''  !!}
            {!!  $model->age ? '<b>Возраст<b> : '. $model->age . '<br>' : ''  !!}
            {!!  $model->from_where ? '<b>Откуда<b> : '. $model->from_where . '<br>' : ''  !!}
            {!!  $model->growth ? '<b>Рост<b> : '. $model->growth . ' см<br>' : ''  !!}
            {!!  $model->weight ? '<b>Вес<b> : '. $model->weight . ' кг<br>' : ''  !!}
            {!!  $model->figure ? '<b>Фигура<b> : '. $model->figure . '<br>' : ''  !!}
            {!!  $model->hair ? '<b>Волосы<b> : '. $model->hair . '<br>' : ''  !!}
            {!!  $model->eye_color ? '<b>Цвет глаз<b> : '. $model->eye_color . '<br>' : ''  !!}
            <br>
            <p>
                {{ $model->description }}
            </p>
        </div>
    </div>
    <br>
    <br>
    <div class="text-center">
        <h4>Фото галерея {{ $model->name }}</h4>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#">
                <img class="img-responsive" src="http://placehold.it/400x300" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#">
                <img class="img-responsive" src="http://placehold.it/400x300" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#">
                <img class="img-responsive" src="http://placehold.it/400x300" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#">
                <img class="img-responsive" src="http://placehold.it/400x300" alt="">
            </a>
        </div>
    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>