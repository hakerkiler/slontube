@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">ТОП 100 актрис и моделей по популярности порно видео на сайте Слон Тюб</div>

                <div class="panel-body">
                    @forelse($models as $model)
                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                            <a class="thumbnail model_view" href="#" data-model-id="{{ $model->id }}">
                                <img class="img-responsive" src="/upload/{{ $model->img }}" alt="">
                            </a>
                        </div>
                    @empty
                        <h1>Please import data into db (model:import)</h1>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modelsView" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">

        </div>
    </div>
</div>


<script>
    var show_model = '{!! route('models.show', 0) !!}'
</script>
@endsection
