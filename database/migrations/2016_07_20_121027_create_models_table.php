<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('img');
            $table->string('link');
            $table->string('name');
            $table->string('dob');
            $table->integer('age');
            $table->string('aliases');
            $table->string('from_where');
            $table->unsignedInteger('growth');
            $table->unsignedInteger('weight');
            $table->string('figure');
            $table->string('hair');
            $table->string('eye_color');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExist('models');
    }
}
